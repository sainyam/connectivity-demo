# Connectivity Demo

## filetree
```
.
├── README.md         # This file
├── connectivity      # python module
│   ├── __init__.py
│   ├── db.py         # contains necessary objects for DB + wrappers
│   ├── endpoint.py   # XML and JSON Endpoints to be called
│   ├── hotel.py      # Hotel Object
│   └── request.py    # Request object containing the search parameters
└── main.py           # Excution logic
```

## How to run

1) Install the python module in requirements.py 
2) make sure that connectivity folder is in the PYTHONPATH
3) run the python file main.py


## Some design decisions

1) Testing would require set-up of request call mockers, which would take considerable time to setup and would take more than the alloted 3-4 hours.
2) Hotel Object is different from the one defined in the API's response to enable easier storage of prices.
3) 
