#!/usr/bin/env python3

import argparse
import copy
from concurrent.futures import ThreadPoolExecutor
from pprint import pprint
from typing import List

from connectivity.db import SqliteDatabaseImpl
from connectivity.endpoint import JSONEndPoint, XMLEndPoint
from connectivity.hotel import HotelObj
from connectivity.request import Request

SNAPTRAVEL_API_URL = "https://experimentation.getsnaptravel.com/interview/hotels"
RETAIL_LEGACY_URL = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"

snaptravel_endpoint = JSONEndPoint(url_str=SNAPTRAVEL_API_URL, api_name="snaptravel")
retail_endpoint = XMLEndPoint(url_str=RETAIL_LEGACY_URL, api_name="retail")


def merge_hotel_lists(
    snaptravel_list: List[HotelObj], legacy_list: List[HotelObj]
) -> List[HotelObj]:
    snaptravel_response_map = {el.id: el for el in snaptravel_list}

    final_result = []
    for el in legacy_list:
        if el.id in snaptravel_response_map:
            hotel_obj = copy.deepcopy(el)
            snaptravel_price = snaptravel_response_map[el.id].get_price(api_type="snaptravel")
            hotel_obj.set_price(price=snaptravel_price, api_type="snaptravel")
            final_result.append(hotel_obj)
    return final_result


def execute_one_request(req):
    pool = ThreadPoolExecutor(2)

    snaptravel_response_future = pool.submit(snaptravel_endpoint, req)
    retail_response_future = pool.submit(retail_endpoint, req)

    snaptravel_response = snaptravel_response_future.result()
    legacy_response = retail_response_future.result()
    final_result = merge_hotel_lists(
        snaptravel_list=snaptravel_response, legacy_list=legacy_response
    )
    return final_result


if __name__ == "__main__":

    def add_arguments(parser):
        parser.add_argument("--city", type=str, required=True)
        parser.add_argument("--checkin", type=str, required=True)
        parser.add_argument("--checkout", type=str, required=True)
        return parser

    parser = argparse.ArgumentParser()
    parser = add_arguments(parser)
    args = parser.parse_args()
    req = Request(checkout=args.checkout, checkin=args.checkin, city=args.city)
    db = SqliteDatabaseImpl(conn_string="/tmp/snaptravel-db.db")
    db.create_sqlite_db()
    result = execute_one_request(args)
    for hotel in result:
        db.add_hotel(hotel)
    print("done")


