import json


class Request(object):
    def __init__(self,
                 city: str,
                 checkin: str,
                 checkout: str,
                 provider: str = "snaptravel"):
        self.city = city
        self.checkin = checkin
        self.checkout = checkout
        self.provider = provider

    def __repr__(self):
        return json.dumps(
            self.__dict__,
            default=lambda o: o.__dict__,
        )
