import json
import logging
import sqlite3
from abc import ABC, abstractmethod

from .hotel import HotelObj


class DatabaseBaseCls(object):
    @abstractmethod
    def init_connection(self, conn_string):
        raise NotImplementedError

    @abstractmethod
    def add_hotel(self, hotel: HotelObj):
        raise NotImplementedError


class SqliteDatabaseImpl(DatabaseBaseCls):
    def __init__(self, conn_string: str):
        self.conn = sqlite3.connect(conn_string)
        self.create_sqlite_db()

    @classmethod
    def _get_hotel_sql(cls):
        sql_get_tasks_table = """
        CREATE TABLE IF NOT EXISTS hotels (
                            id integer PRIMARY KEY,
                            hotel_name text NOT NULL,
                            num_reviews integer NOT NULL,
                            address text NOT NULL,
                            amenities TEXT NOT NULL,
                            image_url text NOT NULL,
                            num_stars integer
                            );
        """
        return sql_get_tasks_table

    @classmethod
    def _get_prices_sql(cls):
        sql_prices_table = """
        CREATE TABLE IF NOT EXISTS hotel_prices (
                            hotel_id integer NOT NULL,
                            price_type text NOT NULL,
                            price real NOT NULL,
                            FOREIGN KEY(hotel_id) REFERENCES hotels(id)
                        );
        """
        return sql_prices_table

    def create_sqlite_db(self):
        cur = self.conn.cursor()
        hotels_sql = self._get_hotel_sql()
        cur.execute(hotels_sql)

        prices_sql = self._get_prices_sql()
        cur.execute(prices_sql)

    def __del__(self):
        self.conn.commit()
        self.conn.close()

    def add_hotel(self, hotel):
        cur = self.conn.cursor()

        INSERT_HOTEL_SQL_TMPL = """
            INSERT INTO hotels (
                id,
                hotel_name,
                num_reviews,
                address,
                amenities,
                image_url,
                num_stars
            ) VALUES (:id,
                :hotel_name,
                :num_reviews,
                :address,
                :amenities,
                :image_url,
                :num_stars
            );
        """

        INSERT_PRICES_SQL = """
                INSERT INTO hotel_prices(
                    hotel_id,
                    price_type,
                    price
                ) VALUES (
                    :hotel_id,
                    :price_type,
                    :price
                )
        """

        try:
            cur.execute(
                INSERT_HOTEL_SQL_TMPL,
                {
                    "id": hotel.id,
                    "hotel_name": hotel.hotel_name,
                    "num_reviews": hotel.num_reviews,
                    "address": hotel.address,
                    "amenities": json.dumps(hotel.amenities),
                    "image_url": hotel.image_url,
                    "num_stars": hotel.num_stars,
                },
            )
        except sqlite3.IntegrityError as err:
            logging.warn("Integrity Error, probably the hotel id already exists", err)

        price_types_available = hotel.get_available_price_types()

        if len(price_types_available) > 0:
            for price_type in price_types_available:
                cur.execute(
                    INSERT_PRICES_SQL,
                    {
                        "hotel_id": hotel.id,
                        "price": hotel.get_price(price_type),
                        "price_type": price_type,
                    },
                )


if __name__ == "__main__":
    print("Sdf")
    test_hotel = HotelObj(
        id=334,
        hotel_name="fsdfsf's",
        num_reviews=34,
        address="dfgdfgdfg",
        amenities=["golf", "ac"],
        image_url="fsdfs",
        num_stars=34,
    )
    test_hotel.set_price(23434.435, "retail")
    test_hotel.set_price(2321434.435, "retail")
    print(test_hotel)
    db = SqliteDatabaseImpl("my_first_db-2.sqlite")
    db.add_hotel(test_hotel)
