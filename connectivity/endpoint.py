import json
import logging
from abc import ABC, abstractmethod
from typing import Any, Dict, List

import requests
from lxml import etree as ET

from .hotel import HotelObj
from .request import Request


class EndPoint(object):
    @abstractmethod
    def __call__(self, request: Request) -> List[HotelObj]:
        raise NotImplementedError("this is abstract class, use the derivatives")


class XMLEndPoint(EndPoint):
    def __init__(self, url_str: str, api_name="retail") -> None:
        self._url = url_str
        self._api_name = "retail"

    @classmethod
    def _extract_amentities_from_xml(cls, hotel_element):
        amenities_list = []
        amenities_element = hotel_element.find("amenities")
        for ammentity in amenities_element:
            amenities_list.append(ammentity.text)
        return amenities_list

    @classmethod
    def request_to_xml(cls, request: Request) -> str:
        root = ET.Element("root")
        ET.SubElement(root, "checkin").text = request.checkin
        ET.SubElement(root, "checkout").text = request.checkout
        ET.SubElement(root, "city").text = request.city
        ET.SubElement(root, "provider").text = "snaptravel"

        return ET.tostring(root, encoding="utf-8", xml_declaration=True)

    def xml_to_list_of_hotel(self, xml_str) -> List[HotelObj]:
        hotel_objs = list()
        xml_tree = ET.fromstring(xml_str)
        for hotel_element in xml_tree:
            try:
                hotel_objs.append(self._parse_hotel_element(hotel_element))
            except ValueError as err:
                logging.error(
                    f"Value Error because of expectations mismatch "
                    + f"encouted xml + {ET.tostring(hotel_element)}",
                    err,
                )
                raise RuntimeError("Value Cannot be parsed")
        return hotel_objs

    def _parse_hotel_element(self, hotel_element):
        hotel_obj = HotelObj(
            id=int(hotel_element.find("id").text),
            hotel_name=hotel_element.find("hotel_name").text,
            num_reviews=int(hotel_element.find("num_reviews").text),
            address=hotel_element.find("address").text,
            amenities=self._extract_amentities_from_xml(hotel_element),
            image_url=hotel_element.find("image_url").text,
            num_stars=int(hotel_element.find("num_stars").text),
        )
        hotel_obj.set_price(float(hotel_element.find("price").text), api_type=self._api_name)
        return hotel_obj

    def __call__(self, request: Request) -> List[HotelObj]:
        request_str = self.request_to_xml(request)
        url_response = requests.post(
            url=self._url, data=request_str, headers={"Content-Type": "application/xml"}
        )
        return self.xml_to_list_of_hotel(url_response.content)


class JSONEndPoint(object):
    def __init__(self, url_str: str, api_name="snaptravel") -> None:
        self._url = url_str
        self._api_name = api_name

    @classmethod
    def request_to_json(cls, request: Request) -> Dict[str, str]:
        return {
            "city": request.city,
            "checkin": request.checkin,
            "checkout": request.checkout,
            "provider": "snaptravel",
        }

    def json_to_list_of_hotel(self, json_string: str) -> List[HotelObj]:
        hotel_objs = list()
        json_obj = json.loads(json_string)
        hotels = json_obj["hotels"]
        for hotel in hotels:
            hotel_obj = HotelObj(**hotel)
            hotel_obj.set_price(price=hotel["price"], api_type=self._api_name)
            hotel_objs.append(hotel_obj)
        return hotel_objs

    def __call__(self, request: Request) -> List[HotelObj]:
        request_str = self.request_to_json(request)
        url_response = requests.post(url=self._url, json=request_str)
        return self.json_to_list_of_hotel(url_response.content)
