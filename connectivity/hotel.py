import json
from typing import List


def todict(obj):
    data = {}
    for key, value in obj.__dict__.iteritems():
        try:
            data[key] = todict(value)
        except AttributeError:
            data[key] = value
    return data


class HotelObj(object):
    def __init__(self, id: int, hotel_name: str, num_reviews: int,
                 address: str, amenities: List[str], image_url: str,
                 num_stars: int, *args, **kwargs):
        self.id = id
        self.hotel_name = hotel_name
        self.num_reviews = num_reviews
        self.address = address
        assert amenities is not None, "amenities can't be None, use empty list instead"
        self.amenities = [el for el in amenities
                          ]  # quick hack to counter side-effects
        self.image_url = image_url
        self.num_stars = num_stars
        self.prices = {}

    def __repr__(self):
        return json.dumps(
            self.__dict__,
            default=lambda o: o.__dict__,
        )

    def set_price(self, price, api_type="snaptravel"):
        self.prices[api_type] = price

    def get_price(self, api_type="snaptravel"):
        return self.prices.get(api_type, None)

    def get_available_price_types(self):
        return list(self.prices.keys())
